import os
import numpy as np

import mask_rcnn.model as modellib
from mask_rcnn.config import Config
from mask_rcnn import visualize
from mask_rcnn.model import log

from utils import label_mask, make_predictions, convert_prediction
import load_data
import dataset
from sklearn.model_selection import train_test_split



# Root directory of the project
ROOT_DIR = os.getcwd()

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.pth")
IMAGENET_MODEL_PATH = os.path.join(ROOT_DIR, "resnet50_imagenet.pth")



DATA_DIR = 'data'
training_path = DATA_DIR + '/stage1_train'
test_path = DATA_DIR + '/stage1_test'
data = load_data.load_images(training_path)
train_data, val_data = train_test_split(data, test_size=0.2, random_state=42)
test_data = load_data.load_images(test_path)


class TrainConfig(Config):
    # Give the configuration a recognizable name
    NAME = "nuclei"

    # Train on 1 GPU and 8 images per GPU. We can put multiple images on each
    # GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
    GPU_COUNT = 2
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # background + nuclei

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    # IMAGE_MIN_DIM = 256
    # IMAGE_MAX_DIM = 512

    # Use smaller anchors because our image and objects are small
    # RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)  # anchor side in pixels

    # Reduce training ROIs per image because the images are small and have
    # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
    # TRAIN_ROIS_PER_IMAGE = 32

    # Use a small epoch since the data is simple
    STEPS_PER_EPOCH = len(train_data)

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = len(val_data)
    USE_MINI_MASK = False


config = TrainConfig()
config.display()



dataset_train = dataset.NucleiDataset()
dataset_train.load_data(train_data)
dataset_train.prepare()

dataset_val = dataset.NucleiDataset()
dataset_val.load_data(val_data)
dataset_val.prepare()



image_ids = np.random.choice(dataset_train.image_ids, 4)
for image_id in image_ids:
    image = dataset_train.load_image(image_id)
    mask, class_ids = dataset_train.load_mask(image_id)
    visualize.display_top_masks(image, mask, class_ids, dataset_train.class_names)



# Create model in training mode
model = modellib.MaskRCNN(config=config,
                          model_dir=MODEL_DIR)
if config.GPU_COUNT:
    model = model.cuda()



# load weights
init_with = "imagenet"  # imagenet, coco, or last

if init_with == "imagenet":
    model.load_weights(IMAGENET_MODEL_PATH)
elif init_with == "coco":
    # Load weights trained on MS COCO, but skip layers that
    # are different due to the different number of classes
    # See README for instructions to download the COCO weights
    model.load_weights(COCO_MODEL_PATH)
elif init_with == "last":
    # Load the last model you trained and continue training
    model.load_weights(model.find_last()[1])



# Train the head branches
# Passing layers="heads" freezes all layers except the head
# layers. You can also pass a regular expression to select
# which layers to train by name pattern.
model.train_model(dataset_train, dataset_val,
            learning_rate=config.LEARNING_RATE,
            epochs=10,
            layers='heads')

