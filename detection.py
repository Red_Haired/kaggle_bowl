import numpy as np
from utils import flatten_mask


def detect(model, image, split=False):
    try:
        results = model.detect([image])
        r = results[0]
    except IndexError:
        return np.zeros_like(image, dtype=np.int32)
    if split and len(r['scores']) > 50:
        h_half = image.shape[0] // 2
        w_half = image.shape[1] // 2

        upper_left = image[:h_half, :w_half, :]
        upper_right = image[:h_half, w_half:, :]
        lower_left = image[h_half:, :w_half, :]
        lower_right = image[h_half:, w_half:, :]
        images = [upper_left, upper_right, lower_left, lower_right]
        masks = []
        num_objects = 0
        for img in images:
            try:
                r = model.detect([img])[0]
            except IndexError:
                masks.append(np.zeros_like(image, dtype=np.int32))
                continue
            mask = flatten_mask(r['masks'], r['scores'])
            mask += (mask != 0) * num_objects
            masks.append(mask)
            num_objects += len(r['scores'])
        upper_left, upper_right, lower_left, lower_right = masks
        blocks = [[upper_left, upper_right], [lower_left, lower_right]]
        cum_mask = np.block(blocks)
    else:
        cum_mask = flatten_mask(r['masks'], r['scores'])
    return cum_mask