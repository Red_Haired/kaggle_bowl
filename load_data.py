import numpy as np
from skimage import io, img_as_float, img_as_ubyte
from skimage import transform as st
from pathlib import Path
from tqdm import tqdm
import torch as t
import torchvision.transforms as tsf
from torchvision.transforms.functional import to_pil_image, to_tensor
import PIL


class Resize(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size
        self.resize_mask = tsf.Resize(output_size, interpolation=PIL.Image.NEAREST)
        self.resize = tsf.Resize(output_size)

    def __call__(self, sample):
        image = sample['image']
        out_sample = sample.copy()

        out_sample['image'] = self.resize(image)
        if 'cum_mask' in sample:
            out_sample['cum_mask'] = self.resize_mask(sample['cum_mask'])

        return out_sample


class RandomCrop(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        image = sample['image']
        out_sample = sample.copy()

        w, h = image.size
        new_h, new_w = self.output_size

        #print(h, w, h - new_h + 1, w - new_w + 1)

        top = np.random.randint(0, h - new_h + 1)
        left = np.random.randint(0, w - new_w + 1)

        out_sample['image'] = image.crop((left, top, left + new_w, top + new_h))
        if 'cum_mask' in sample:
            out_sample['cum_mask'] = sample['cum_mask'].crop((left, top, left + new_w, top + new_h))
        return out_sample


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image = sample['image']
        out_sample = sample.copy()
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        #image = image.transpose((2, 0, 1))
        out_sample['image'] = to_tensor(sample['image'])#t.from_numpy(image)
        if 'cum_mask' in sample:
            out_sample['cum_mask'] = to_tensor(sample['cum_mask'])#t.from_numpy(sample['cum_mask'].transpose((2, 0, 1)))
        return out_sample


class ToPILImage(object):
    """Convert ndarrays in sample to PIL images."""
    def __call__(self, sample):
        out_sample = sample.copy()
        out_sample['image'] = to_pil_image(sample['image'], mode='RGB')
        if 'cum_mask' in sample:
            out_sample['cum_mask'] = to_pil_image(sample['cum_mask'])
        return out_sample


class Normalize(object):
    """ Normalize image"""

    def __call__(self, sample):
        image = sample['image']
        out_sample = sample.copy()
        image = image.permute(1, 2, 0)
        min_vals = image.min(dim=0)[0].min(dim=0)[0]
        max_vals = image.max(dim=0)[0].max(dim=0)[0]
        image = (image - min_vals) / (max_vals - min_vals)
        image = (image - 0.5)*2
        out_sample['image'] = image.permute(2, 0, 1)
        if 'cum_mask' in sample:
            mask = t.zeros_like(sample['cum_mask'])
            out_sample['mask'] = 1 - t.eq(sample['cum_mask'], 0, out=mask)
        return out_sample


def load_mask(mask_path):
    masks = []
    for file in sorted(list(mask_path.iterdir())):
        mask = io.imread(file, as_grey=True)
        mask = (mask / 255.0).astype(np.float32)
        mask = mask[:, :, None]
        masks.append(mask)
    return masks


def normalize(img):
    img = img_as_float(img)
    min_vals = img.min(axis=0).min(axis=0)
    max_vals = img.max(axis=0).max(axis=0)
    img = (img - min_vals) / (max_vals - min_vals)
    img = img_as_ubyte(img)
    return img


def load_images(file_path, limit=None):
    paths = list(Path(file_path).iterdir())
    data = []
    for file in tqdm(paths, total=len(paths)):
        item = {}
        image_name = list((file/'images').iterdir())
        assert len(image_name) == 1
        image_name = image_name[0]
        img = io.imread(image_name)
        if len(img.shape) == 2:
            img = np.dstack((img, img, img))
        img = img[:, :, :3]
        img = normalize(img)
        item['image'] = img
        item['name'] = file.name
        item['height'] = img.shape[0]
        item['width'] = img.shape[1]
        if (file/'masks').exists():
            masks = load_mask(file/'masks')
            w = range(1, len(masks) + 1)
            cum_mask = np.tensordot(w, masks, 1).astype(np.uint8)
            item['cum_mask'] = cum_mask
            item['object_count'] = len(masks)
        data.append(item)
        if len(data) == limit:
            break
    return data


class Dataset():
    def __init__(self, data, transform):
        self.datas = data
        self.transform = transform

    def __getitem__(self, index):
        sample = self.datas[index]
        sample['idx'] = index
        return self.transform(sample)

    def __len__(self):
        return len(self.datas)
