import torch as t
import numpy as np
from scipy import ndimage
import torchvision.transforms as tsf
import PIL



def label_mask(mask, min_obj_size=10):
    label, num_features = ndimage.label(mask)
    object_ids, counts = np.unique(label, return_counts=True)
    for id, count in zip(object_ids[1:], counts[1:]):
        if count < min_obj_size or count/(mask.shape[0]*mask.shape[1]) > 0.15:
            label *= mask != id
    return label


def convert_prediction(o, batch, cut=0.5):
    predictions = []
    probs = []
    # Loop through the batch
    for idx in range(len(o)):
        tm = o[idx][0].data.cpu().numpy()[:, :, None]
        labels = label_mask(tm > cut)
        size = (batch['height'][idx], batch['width'][idx])
        resizer = tsf.Compose([tsf.ToPILImage(), tsf.Resize(size, interpolation=PIL.Image.NEAREST), tsf.ToTensor()])
        tm = (tm * 255).astype('uint8')

        labels = resizer(labels)[0].numpy()
        tm = resizer(tm)[0].numpy()
        predictions.append(labels)
        probs.append(tm)
    return predictions, probs


def make_predictions(model, dataloader, use_cuda, cut=0.5):
    model = model.eval()
    indices = []
    names = []
    masks = []
    tms = []
    for sample in dataloader:
        image = sample['image']
        if use_cuda:
            image = image.cuda()
        x = t.autograd.Variable(image, volatile=True)  # .cuda())
        o = model(x)
        labels, tm = convert_prediction(o, sample, cut)
        for i in range(len(o)):
            tms.append(tm[i])
            masks.append(labels[i])
            names.append(sample['name'][i])
            indices.append(sample['idx'][i])
    return names, masks, tms, indices


def flatten_mask(masks, scores):
    cum_mask = np.zeros_like(masks[:, :, 0], dtype=np.int32)
    masks = np.transpose(masks, (2, 0, 1))
    for i, mask, score in zip(range(1, len(scores) + 1), masks, scores):
        intersection = np.logical_and(cum_mask, mask)
        mask = np.logical_and(mask, np.invert(intersection))
        cum_mask += mask * i

    np.transpose(masks, (1, 2, 0))
    return cum_mask
