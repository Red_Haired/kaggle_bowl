from load_data import load_images
from mask_rcnn.utils import Dataset
import numpy as np


class NucleiDataset(Dataset):
    def load_data(self, data):
        self.data = data
        self.add_class("nuclei", 1, "nuclei")
        for i in range(len(data)):
            self.add_image("nuclei", image_id=i, path=data[i]['name'])

    def load_image(self, image_id):
        return self.data[image_id]['image']

    def image_reference(self, image_id):
        info = self.image_info[image_id]
        if info["source"] == "nuclei":
            return info["path"]
        else:
            super(self.__class__).image_reference(self, image_id)

    def load_mask(self, image_id):
        cum_mask = self.data[image_id]['cum_mask']
        count = self.data[image_id]['object_count']
        mask = np.zeros([self.data[image_id]['height'], self.data[image_id]['width'], count], dtype=np.bool)
        for i in range(count):
            mask[:, :, i] = (cum_mask[:, :, 0] == i + 1)
        return mask, np.ones(count, dtype=np.int32)
