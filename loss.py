import numpy as np

def soft_dice_loss(inputs, targets):
    num = targets.size(0)
    m1 = inputs.view(num,-1)
    m2 = targets.view(num,-1)
    intersection = (m1 * m2)
    score = 2. * (intersection.sum(1)+1) / (m1.sum(1) + m2.sum(1)+1)
    score = 1 - score.sum()/num
    return score


def image_score(predictions, targets):
    predictions = predictions.astype(np.int32)
    targets = targets.astype(np.int32)
    thresholds = np.linspace(0.5, 0.95, 10)
    target_labels, t_counts = np.unique(targets, return_counts=True)
    target_labels, t_counts = target_labels[:-1], t_counts[:-1]
    predict_labels, p_counts = np.unique(predictions, return_counts=True)
    predict_labels, p_counts = predict_labels[:-1], p_counts[:-1]
    # compute intersections of all predictions with all targets
    un_mask = predictions*1000 + targets

    un_labels, counts = np.unique(un_mask, return_counts=True)
    # areas for intersections of prediction and target
    un_dict = dict(zip(un_labels, counts))
    # areas of targets
    t_dict = dict(zip(target_labels, t_counts))
    # areas of predictions
    p_dict = dict(zip(predict_labels, p_counts))
    TPs = np.zeros_like(thresholds)
    t_nomatch = {}
    p_nomatch = {}
    for tl in target_labels:
        for pl in predict_labels:
            ul = pl*1000 + tl
            intersetion_area = un_dict.get(ul, 0)
            union_area = t_dict[tl] + p_dict[pl] - intersetion_area
            iou = float(intersetion_area) / union_area
            matches = (iou > thresholds)
            TPs += matches
            t_nomatch[tl] = np.invert(matches) & t_nomatch.get(tl, np.ones_like(thresholds, dtype=bool))
            p_nomatch[pl] = np.invert(matches) & p_nomatch.get(pl, np.ones_like(thresholds, dtype=bool))

    FPs = np.sum(list(p_nomatch.values()), axis=0)
    FNs = np.sum(list(t_nomatch.values()), axis=0)
    if len(predict_labels) == 0:
        FNs = len(target_labels)
    #print('predict_labels', len(predict_labels))
    #print('target_labels', len(target_labels))
    #print('TPs', TPs)
    #print('FPs', FPs)
    #print('FNs', FNs)
    scores = TPs/(TPs + FPs + FNs)
    return np.mean(scores)


def score(predictions, targets):
    scores = []
    for prediction, target in zip(predictions, targets):
        scores.append(other_score(prediction, target))
    return np.mean(scores)


def other_score(y_pred, labels):
    # Compute number of objects
    true_objects = len(np.unique(labels))
    pred_objects = len(np.unique(y_pred))
    #print("Number of true objects:", true_objects)
    #print("Number of predicted objects:", pred_objects)

    # Compute intersection between all objects
    intersection = np.histogram2d(labels.flatten(), y_pred.flatten(), bins=(true_objects, pred_objects))[0]

    # Compute areas (needed for finding the union between all objects)
    area_true = np.unique(labels, return_counts=True)[1]
    area_pred = np.unique(y_pred, return_counts=True)[1]
    area_true = np.expand_dims(area_true, -1)
    area_pred = np.expand_dims(area_pred, 0)

    # Compute union
    union = area_true + area_pred - intersection

    # Exclude background from the analysis
    intersection = intersection[1:, 1:]
    union = union[1:, 1:]
    union[union == 0] = 1e-9

    # Compute the intersection over union
    iou = intersection / union

    # Precision helper function
    def precision_at(threshold, iou):
        matches = iou > threshold
        true_positives = np.sum(matches, axis=1) == 1  # Correct objects
        false_positives = np.sum(matches, axis=0) == 0  # Missed objects
        false_negatives = np.sum(matches, axis=1) == 0  # Extra objects
        tp, fp, fn = np.sum(true_positives), np.sum(false_positives), np.sum(false_negatives)
        return tp, fp, fn

    # Loop over IoU thresholds
    prec = []
    #print("Thresh\tTP\tFP\tFN\tPrec.")
    for t in np.arange(0.5, 1.0, 0.05):
        tp, fp, fn = precision_at(t, iou)
        p = tp / (tp + fp + fn)
        #print("{:1.3f}\t{}\t{}\t{}\t{:1.3f}".format(t, tp, fp, fn, p))
        prec.append(p)
    #print("AP\t-\t-\t-\t{:1.3f}".format(np.mean(prec)))
    return np.mean(prec)


if __name__ == '__main__':
    import skimage.io
    import skimage.segmentation
    import matplotlib.pyplot as plt

    # Load a single image and its associated masks
    id = '0a7d30b252359a10fd298b638b90cb9ada3acced4e0c0e5a3692013f432ee4e9'
    file = "data/stage1_train/{}/images/{}.png".format(id, id)
    masks = "data/stage1_train/{}/masks/*.png".format(id)
    image = skimage.io.imread(file)
    masks = skimage.io.imread_collection(masks).concatenate()
    height, width, _ = image.shape
    num_masks = masks.shape[0]

    # Make a ground truth label image (pixel value is index of object label)
    labels = np.zeros((height, width), np.uint16)
    for index in range(0, num_masks):
        labels[masks[index] > 0] = index + 1

    # Show label image
    fig = plt.figure()
    plt.imshow(image)
    plt.title("Original image")
    fig = plt.figure()
    plt.imshow(labels)
    plt.title("Ground truth masks")

    # Simulate an imperfect submission
    offset = 2  # offset pixels
    y_pred = labels[offset:, offset:]
    y_pred = np.pad(y_pred, ((0, offset), (0, offset)), mode="constant")
    y_pred[y_pred == 20] = 0  # Remove one object
    y_pred, _, _ = skimage.segmentation.relabel_sequential(y_pred)  # Relabel objects

    # Show simulated predictions
    fig = plt.figure()
    plt.imshow(y_pred)
    plt.title("Simulated imperfect submission")

    print('image_score', image_score(y_pred, labels))
    print('other_score', other_score(y_pred, labels))
