import numpy as np
from scipy import ndimage
import pandas as pd


def rle_encoding(x):
    '''
    x: numpy array of shape (height, width), 1 - mask, 0 - background
    Returns run length as list
    '''
    dots = np.where(x.T.flatten()==1)[0] # .T sets Fortran order down-then-right
    run_lengths = []
    prev = -2
    for b in dots:
        if (b>prev+1): run_lengths.extend((b+1, 0))
        run_lengths[-1] += 1
        prev = b
    return " ".join([str(i) for i in run_lengths])


def create_submission(img_names, masks, filename='submission.csv'):
    with open(filename, 'w') as f:
        f.write('ImageId,EncodedPixels\n')
        for name, mask in zip(img_names, masks):
            object_ids = np.unique(mask)
            # encode all detected objects except background
            if len(object_ids) == 1:
                f.write("%s,\n" % name)
            for id in object_ids[1:]:
                enc_str = rle_encoding(np.where(mask == id, 1, 0))
                f.write("%s,%s\n" % (name, enc_str))
